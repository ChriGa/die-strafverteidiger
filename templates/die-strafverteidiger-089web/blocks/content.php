<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 9;
if ($this->countModules('left') && $this->countModules('right')) $span = 6;
if (!$this->countModules('left') && !$this->countModules('right')) $span = 12;

/*joomla Model aufrufen um item detials verfügbar zu machen:*/
if($detectAgent == "phone " && $menu->getActive() == $menu->getDefault()) { 
	$model = JModelList::getInstance('Articles', 'ContentModel', array('ignore_request'=>true));
	$appParams = JFactory::getApplication()->getParams();
	$model->setState('params', $appParams);
	$items = $model->getItems();
	$phoneStart = true;
	foreach ($items as $item) {
		if($item->id == "15") { $phoneStartContent = '<div class="phone__anwaltWrapper">' . $item->introtext . '</div>'; } // CG: ACHTUNG id 15 = Startseite mobile Beitrag wenn neuer Beitrag, dann ID ändern!
	}
}
?>
<? //preprint($items); die();?>
<div class="content">
	<jdoc:include type="modules" name="banner" style="xhtml" />
	<div class="row-fluid">
	
		<?php if ($this->countModules('left')) : ?>
			<div class="span3 left">
				<jdoc:include type="modules" name="left" style="xhtml" />
			</div>				
		<?php endif; ?>
		
		<main id="content" role="main" class="span<?php echo $span; ?>">
		<!-- Begin Content -->
		<jdoc:include type="modules" name="position-3" style="xhtml" />
		<jdoc:include type="message" />
<?php if($phoneStart) :
	//Und dann Artikeltitel ausgeben zB Startseite Mobile:
		print $phoneStartContent; ?>

<script type="text/javascript">
	jQuery(function(){
		jQuery('.startNavItem__anwaelte h2').click("on", function(){
			jQuery('.startNavItem__anwaelte').addClass('open');
			jQuery('.startNavItem__link').addClass('shrink');
		});
		jQuery('.closeItem').click("on", function(){
			jQuery('.startNavItem__anwaelte').removeClass('open');
			jQuery('.startNavItem__link').removeClass('shrink');
		});		

	});
</script>
	<?php else : ?>
		<jdoc:include type="component" />
<?php 
	endif; 
?>
		<jdoc:include type="modules" name="position-2" style="none" />
		<!-- End Content -->
		</main>
		
		<?php if ($this->countModules('right')) : ?>
			<div class="span3 right">		
				<jdoc:include type="modules" name="right" style="xhtml" />
			</div>
		<?php endif; ?>
		
	</div>
</div>
	<?php if ($menu->getActive() == $menu->getDefault()) : ?>
		<jdoc:include type="modules" name="startHover" style="custom" />
	<?php endif; ?>
