<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
	
<header id="header" class="fullwidth">
	<div class="flex spaceBetween">        			
		<div class="flexItem siteName">
			<a class="brand" href="<?php echo $this->baseurl; ?>">
					<?php echo '<div class="site-description">
									<h2>' . htmlspecialchars($this->params->get('sitedescription')) . '</h2>
								</div>'; ?>
			</a>
		</div>
		<div class="flexItem last">
			<div class="vcard clr">	
				<h3 class="vcardHeader">Notruf 24/7</h3>
					<p class="tel "><a class="" href="tel:+49899972750">089-99 72 750</a></p>			  
					<p class="org "><span>Die Strafverteidiger</span> Strafrecht</p>
					<p class="adr"><span class="street-address">Ismaninger Straße 102 - 106 </span><br />
				    <span class="postal-code">81675  </span><span class="region">M&uuml;nchen</span>
				  </p>
			</div>
		</div>	          
	</div>
</header>