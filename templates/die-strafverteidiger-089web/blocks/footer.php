<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer footerBanner">
	<div id="copyright">
		<p>&copy; <?php print date("Y");?> Die Strafverteidiger | <a class="imprLink" href="/impressum.html" title="Impressum FIRMA">Impressum</a></p>
	</div>	
	<jdoc:include type="modules" name="footer" style="none" />
</footer>
		