<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>

<html lang="de-De">
<head>
	<?php /* CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS 
		<link href="https://fonts.googleapis.com/css?family=Michroma" rel="stylesheet"> */ ?>
	<?php /*
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/woff2" href="/templates/089-template/fonts/XXX.woff2">
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/woff2" href="/templates/089-template/fonts/XXX.woff2">		
	*/ ?>	
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php print $detectAgent . ($clientMobile ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<div id="aboveTheFold">
				<?php			

				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');						

				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
										
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');
				
				if (strpos($pageclass, "raDetail")) : ?>
					<jdoc:include type="modules" name="anwaltDescModule" style="custom" />
				<? endif;
						
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');				
			?>
			</div>
			<div id="belowTheFold">
			<?php	
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
				
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
				?>
			</div>
		</div>
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script type="text/javascript">

		window.onload = function () {
			setTimeout(function(){
				jQuery('body').addClass('ready');
			}, 500 ); 
		}
	<?php if ($menu->getActive() == $menu->getDefault()) : //jquery anwaltHover function ?>
			var inHoverStyle = {'opacity':1, 'transform': 'scale(1)'};
			var outHoverStyle = {'opacity':0, 'transform':'scale(0)'};
			var raImgToHover = {
									".raWagner img": ".anwaltHover.pw",
									".raZur img": ".anwaltHover.fz",
									".raBeyer img": ".anwaltHover.rb",
									".raLickleder img": ".anwaltHover.al",
									".raMansdoerfer img": ".anwaltHover.mm",
									".raLang img": ".anwaltHover.rl"
								};
			jQuery(function(){
					setTimeout(function() {
						jQuery.each(raImgToHover, function(img, raHover) {
							jQuery(img + ", " + raHover).hover(function(){
								jQuery(raHover).css(inHoverStyle); 
							}, function() {
								jQuery(raHover).css(outHoverStyle);
							});
						});
					}, 2000 );
			});
	<?php endif; //<---- END jquery anwaltHover function?>


		jQuery(document).ready(function() {
			
		<?php // smooth scrolling für anchor up/down un AnwaltDetailansicht: ?>
			jQuery(document).on('click', 'a[href^="#"]', function (event) {
			    event.preventDefault();

			    jQuery('html, body').animate({
			        scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top
			    }, 750, 'swing' );
			});
		<?php //<--- END smoothScroll ?>			

			<?php /*if ($detect->isMobile()) : ?> //mobile

					jQuery.extend(jQuery.lazyLoadXT, { // lazyLoad
						  edgeY:  100,
						  srcAttr: 'data-src'
						});		
					<?php else : ?> //desktop
						jQuery.extend(jQuery.lazyLoadXT, {
							  edgeY:  50,
						  	srcAttr: 'data-src'
						});    

			<?php endif; */ ?>

		//console.log(window.devicePixelRatio);
	});

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>
